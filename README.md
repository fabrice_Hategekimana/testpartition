# Test case partitionning

Pour rappel, le test case partitionning fait partis des tests qui essayent de réaliser un maximum de couvertur du code avec un minimum de teste.

## Exercices:

### Exercice 1:
Les nombres naturels.

Les entiers naturels sont un ensemble de nombre entiers strictement positifs allant de 0 à l'infini. Nous définissons les 4 opération de bases:

- addition
- multiplication
- soustraction
- division

Une particularité de ce genre d'opération sur cet ensemble vient du fait que la soustraction A-B doit renvoyer zéro si B est plus grand que A et que la division A/B doit toujours donner un nombre entier (plus spécifiquement la parti entière de la division). De plus, la division par zéro doit retourner une erreur.

En plus de ces comportements vient aussi s'ajouter la restriction sur la borne maximale des calculs qui sera le signe infini. 

#### Exercice 1.1:
Table de partition.

Le but est de faire, une table qui partitionne chaque interval de valeur possible où on y trouvera des valeurs valides ou des valeurs invalides. Chaque fonction pourra avoir une ligne ou vous pouvez le faire dans une table différentes

C'est un tableau qui ressemble à peu près à ça:

| [-inf, 0]] | [0, 100]   | ... | [num, inf]   |
|------------|------------|-----|--------------|
| invalide   | une valeur | ... | autre valeur |

#### Exercice 1.2:
Implémentation des tests (Javascript).

Maintenant il faut implémenter les tests. L'avantage est que chaque colonne du tableau fait au point précédent peut faire l'objet d'un teste.
vous trouverez le code des quatres opération dans le dossier "naturels".

On ne peut pas representer +inf et -inf en réalité (en effet, la mémoire d'un ordinateur n'est pas infini et il y a une valeur maximum qu'on peut atteindre). Cependant, on peut utiliser les bornes positives et négatives maximales entières de javascript.

| math | javascript              |
|------|-------------------------|
| +inf | Number.MIN_SAFE_INTEGER |
| -inf | Number.MAX_SAFE_INTEGER |

Créez les tests à l'aide du module de test Jest de Javascript et modifier les fonctions pour qu'elles passent ces tests.

### Exercice 2:
résultat d'un concours.

Il y a un courours de developpement qui test les compétence des participant par des épreuves et exercices rapportant des points allant jusqu'à 100. Les participants peuvent s'inscrire à partir de 12 ans.
On veut créer une fonction qui prend en entrée les points d'un partipant ainsi que son age et qui retourne une liste de prix.

Nous avons ces informations:

- les participants qui ont 50 points ou plus reçoivent un bon de réduction pour un magasin electronique de leur choix.
- les participants qui ont moins de 15 ans, reçoivent une invitation au prochain concours.
- les participants entre 20 et 30 ans ayant 80 points ou plus reçoivent une petite bourse de 50CH.
- les participants de moins de 18 ans ayant 60 points ou plus reçoivent un cadeau surprise.
- les participants au delà de 30 ans ayant 90 points ou plus reçoivent une réduction sur un magazine informatique.
- les participants ayant 95 points ou plus reçoivent une médaille.
- les participants ayant 30 points à 49 points reçoivent une lettre d'encouragement.

Bien-sûr, la fonction doit envoyer une erreur pour toute valeurs ne faisant pas partie du champ.

On part du principe que l'age de participation et limité à 100 ans vers le haut et qu'on ne peut pas avoir des points au delà de 100 et en dessous de zéro.

#### Exercice 2.1
Le but est de faire une table qui partionne chaque ensemble de valeurs et indique les prix à recevoir selon les valeurs obtenue.
Il est recommander de placer les intervals des points obtenue en colonne et de mettre les intervals d'age en ligne.

Le tableau devrai ressembler à quelque chose comme ça:

|           | [-inf, 0] | [0, 30] | ... |
|-----------|-----------|---------|-----|
| [-inf, 0] | erreur    | ...     | ... |
| [0,12]    | ...       | ...     | ... |
| ...       | ...       | ...     | ... |

#### Exercice 2.2

Implémentation des tests.

Il faut maintenant créer les tests pour cette fonctions. Vous trouverez le code de cette fonction dans le dossier "concour". Il suffira d'implémenter les testes avec le module Jest de Javascript et de modifier le code si nécessaire pour qu'il les passe.

### Exercice 3
On souhaite faire un formulaire d'enregistrement pour un site qui doit contenir ces champs:
- Nom [String]
- Prénom [String]
- Age [Int]
- Mail [String]

On ne veut pas que l'utilisateur entre des informations invalides et on voudrait le vérifier à l'aide d'une fonction de verification.
Pour ce faire, nous allons utiliser la méthode de teste par partitionnement pour couvrir les situations possibles.
Pour chaque champs, il faut évaluer les domaines de valeurs admises ou non en s'appuyant sur ces informations:

- Le mail ne peut pas être une chaîne de caractère vide
- Le nom et le prénom peuvent être optionnel (chaîne vide)
- L'age d'une personne ne peut être négative et on limite à 100 ans 
- Un mail doit être structuré en [part].[part]@[part].[com|ch]
- Il est recommandé de créer des fonctions de vérification pour chaque champ qui vont être appelé par la fonctions principale
- La fonction doit retourner vrai (true) ou faux (false) si la validation passe ou ne passe pas.

#### Exercice 3.1
Créer, si nécessaire, un tableau de partitionnement pour les champs mentionnés.

#### Exercice 3.2
Implémentez les tests ainsi que la fonction de vérification.
Vous trouverez les fichiers dans le dossier "formulaire".





