//opération sur les naturels
//
//Ces fonctions sont simple mais ne correspondent pas aux normes pour faire des opérations sur les entiers. De plus, ces fonctions ne sont pas sécurisés contre les mauvaises utilisation du code.


function addition(A,B){
	return A + B;
}

function multiplication(A,B){
	return A * B;
}

function soustraction(A,B){
	return A - B;
}

function division(A,B){
	return A / B;
}

export { addition, multiplication, soustraction, division };
